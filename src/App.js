import React from 'react';
import Header from './components/header';
import Block from './components/block';
import './App.scss';

function App() {
  return (
    <div>
      <div className="container">
        <Header />
      
        <div className="blocks_container">
          <Block />
          <Block />
          <Block />
        </div>
      </div>
    </div>
  );
}

export default App;
