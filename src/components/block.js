import React, { useState } from 'react';
import { useFetch } from '../hooks';

import axios from 'axios';

function Block() {

  const [blockData, setBlockData] = useState({
    resultLimit: 15,
    selectedValue: null,
    selected: false,
    selectedBeers: null
  })

  const [data] = useFetch(
    "http://ontariobeerapi.ca/beers/"
  );

  

  const brewers = Array.from(new Set(data.map(a => a.brewer)))
    .map(brewer => {
      return data.find(a => a.brewer === brewer)
  })

  
  function getBeers(e) {
      setBlockData({
        loadMore: true,
        selected: true,
        resultLimit: 15,
        selectedValue: e
      })
  }

  if (blockData.selectedValue) {
    axios.get(`http://ontariobeerapi.ca/beers/?brewer=${blockData.selectedValue}`, { crossdomain: true })
      .then(function (response) {
        setBlockData({
          selectedBeers: response.data
        })
      })
  }

  // Add default image if beer image does not exist on the server
  function addDefaultSrc(ev){
    ev.target.src = 'https://dummyimage.com/200x200/000/fff'
  }

  return(
    <div className="block">
      <p className="heading">Select Brewer</p>
        {
          <select onChange={(e) => {getBeers(e.target.value)}}>
            <option>Select</option>
            {
            brewers.map(({ beer_id, brewer }) => (
              <option key={beer_id} value={brewer}>
                {brewer}
              </option> 
            )).sort()}
          </select>
        }

        {
            <ul>
              {
              blockData.selectedBeers ?
              blockData.selectedBeers.map(item => (
                <li key={item.id}>
                  <img onError={addDefaultSrc} src={item.image_url} alt={item.name} />
                  <p>{item.name}</p>
                  <p>{item.price} PLN</p>
                </li>
              )).slice(0, 15) :
              ''
              }  
          </ul>
        }
            
        {/* <div className={ blockData.loadMore ? 'visible more' : 'hidden more' }>
          <button className="load_more" onClick={loadMore}>
            Load more
          </button>
        </div> */}
    </div>
  )
}

export default Block;
